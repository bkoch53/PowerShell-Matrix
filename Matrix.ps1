Param(
	$Color = "Green",
	$Drops = 5,
	$Grow = $True,
	$BackgroundColor = $Host.UI.RawUI.BackgroundColor
)

Class Drop {
	[System.Collections.ArrayList]$Chars
	[int]$X
	[int]$Height
	[int]$Speed
	Drop($Height,$Width) {
		$this.X = Get-Random -Max $Width
		$this.Height = $Height
		$Y = Get-Random -Max $Height
		$this.Speed = Get-Random -min 1 -max 3
		$NewChars = @()
		$NewChars += 1..$this.Speed | %{
			New-Object -TypeName PSObject -Property @{
				Letter = " "
				Y = $(-20 - $_) + $Y
			}
		}
		$NewChars += -20..0 | %{
			New-Object -TypeName PSObject -Property @{
				Letter = [char]$(Get-Random -min 65 -max 150)
				Y = $_ + $Y
			}
		}
		[System.Collections.ArrayList]$this.Chars = $NewChars
	}
	Update() {
		$this.Chars[-1].Letter = [char]$(Get-Random -min 65 -max 150)
		ForEach ($Char in $this.Chars) {
			$Char.Y = $Char.Y + $this.Speed
			If ($Char.Y -ge $this.Height) {
				$Char.Y = 0
			}
		}
	}
}
Function Setup($Drops,$BackgroundColor) {
	$Host.UI.RawUI.BackgroundColor = $BackgroundColor
	$Global:Width = $Host.UI.RawUI.WindowSize.Width
	$Global:Height = $Host.UI.RawUI.WindowSize.Height - 3
	$Global:Drops = 1..$Drops | % {
		[Drop]::New($Global:Height,$Global:Width)
	}
	[System.Collections.ArrayList]$Global:Drops = $Global:Drops
	cls
}
Function Draw($Color) {
	ForEach ($Drop in $Global:Drops) {
		$Host.UI.RawUI.CursorPosition = @{X=$Drop.X;Y=$Drop.Chars[-1].Y}
		$Host.UI.RawUI.ForegroundColor = $Color
		Write-Output $Drop.Chars.Letter[-1] -ErrorAction SilentlyContinue
		$Host.UI.RawUI.ForegroundColor = "Dark$Color"
		ForEach ($Char in ($Drop.Chars | Select -First 21)) {
			Try {
				$Host.UI.RawUI.CursorPosition = @{X=$Drop.X;Y=$Char.Y}
			} Catch {}
			Write-Output $Char.Letter -ErrorAction SilentlyContinue
		}
	}
}
Function Main {
	Setup $Drops $BackgroundColor
	While ($True) {
		If ($Global:Drops.Count -le 50) {
			$Global:Drops.Update()
		}
		Draw $Color
		If (($Grow) -and (((Get-Random) % 6) -eq 0)) {
			$Global:Drops.Add([Drop]::New($Global:Height,$Global:Width))
		}
	}
}
Try {
	$OFG = $Host.UI.RawUI.ForegroundColor
	$OBG = $Host.UI.RawUI.BackgroundColor
	Main
}
Catch {}
Finally {
	$Host.UI.RawUI.ForegroundColor = $OFG
	$Host.UI.RawUI.ForegroundColor = $OBG
	$Global:Drops = $Null
}
